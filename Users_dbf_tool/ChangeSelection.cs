﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Users_dbf_tool
{
    public partial class ChangeSelection : Form
    {
        string type;
        public ChangeSelection(string type )
        {
            InitializeComponent();
            this.type = type;
            if(type == "KEY")
            {
                textBox1.Text = MainWindow.dbf.SelectedDbf.Pswd;
                textBox2.Select();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            switch (type){
                case "PATH":
                        foreach (DataRow row in MainWindow.dbf.DS.Tables["DatasDbf"].Rows)
                        {
                            row["PATH"] = row["PATH"].ToString().Replace(textBox1.Text, textBox2.Text);
                        }
                    this.Close();
                    break;

                case "KEY":
                    if (!Model.ADS.decrypted)
                    {
                        try { Model.ADS.decryptDbf(MainWindow.dbf.SelectedDbf.Path, MainWindow.dbf.SelectedDbf.Pswd, MainWindow.dbf.SelectedDbf.Name.Split('.').First()); } catch (Exception ex) { MessageBox.Show(ex.Message); return; }
                    }
                    MainWindow.dbf.SelectedDbf = Model.DBFVM.createDbf(MainWindow.dbf.SelectedDbf.Path, textBox2.Text, MainWindow.dbf.SelectedDbf.Name);
                    Model.ADS.encryptDbf(MainWindow.dbf.SelectedDbf.Path, MainWindow.dbf.SelectedDbf.Pswd, MainWindow.dbf.SelectedDbf.Name.Split('.').First());
                    //MessageBox.Show("Remplacement effectué");
                    this.Close();
                    break;
            }
        }
    }
}
