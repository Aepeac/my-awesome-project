﻿using Advantage.Data.Provider;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Users_dbf_tool
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static Model.DBFVM dbf;
        public MainWindow()
        {
            InitializeComponent();
            dbf = new Model.DBFVM();
            this.DataContext = dbf;

            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show(dbf.Path);
            //return;
            if (dbf.SelectedDbf.Path.Equals(""))
            {
                MessageBox.Show("Veuillez indiquer le chemin du Users.dbf");
                return;

            }
            try
            {
                File.Copy(dbf.SelectedDbf.Path + @"\" + dbf.SelectedDbf.Name, dbf.SelectedDbf.Path + @"\" + "dbbtemp_users.dbf",true);
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            //if (!Model.ADS.decrypted)
            //{
            //    try { Model.ADS.decryptDbf(dbf.SelectedDbf.Path, dbf.SelectedDbf.Pswd, dbf.SelectedDbf.Name.Split('.').First()); } catch (Exception ex) { MessageBox.Show(ex.Message); return; }
            //}

            //+"encryptionpassword=" + dbf.Pswd + ";" + "Password=" + dbf.Pswd + ";"
            //AdsConnection conn = new AdsConnection(Model.ADS.makeConnectionString(dbf.SelectedDbf.Path + @"\" + dbf.SelectedDbf.Name , dbf.SelectedDbf.Pswd));


            dbf.DS = Model.ADS.getDataTable(dbf.SelectedDbf.Name.Split('.').First(), dbf.SelectedDbf.Path, dbf.SelectedDbf.Pswd);
            dbf.Update = true;
        }


        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (Directory.Exists(dbf.SelectedDbf.Path))
            {
                DirectoryInfo directory = new DirectoryInfo(dbf.SelectedDbf.Path);
                var dbfs = from FileInfo file in directory.GetFiles()
                           where file.FullName.ToUpper().Contains(".DBF")
                           select file.Name;
                ObservableCollection<string> temp = new ObservableCollection<string>();
                foreach(string s in dbfs.ToList())
                {
                    temp.Add(s);
                }
                dbf.DBFS = temp;
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            dbf.Update = false;
            if (!Model.ADS.decrypted)
            {
                try { Model.ADS.decryptDbf(dbf.SelectedDbf.Path, dbf.SelectedDbf.Pswd, dbf.SelectedDbf.Name.Split('.').First()); } catch (Exception ex) { MessageBox.Show(ex.Message); return; }
            }

            Model.ADS.UpdateData( dbf.SelectedDbf.Name.Split('.').First(), dbf.DS, dbf.SelectedDbf.Path, dbf.SelectedDbf.Pswd);
            Model.ADS.encryptDbf(dbf.SelectedDbf.Path, dbf.SelectedDbf.Pswd, dbf.SelectedDbf.Name.Split('.').First());
            dbf.Update = true;
        }

        private void Button_Click_2(object sender, RoutedEventArgs e) 
        {
            this.Close();
        }

        protected override void OnClosed(EventArgs e)
        {
            if (Model.ADS.decrypted) { Model.ADS.encryptDbf(dbf.SelectedDbf.Path, dbf.SelectedDbf.Pswd, dbf.SelectedDbf.Name.Split('.').First()); }
            try
            {
                File.Delete(dbf.SelectedDbf.Path + @"\" + "dbbtemp_users.dbf");
            }
            catch (Exception ex)
            {
                
            }
            base.OnClosed(e);
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e) // path 
        {
            ChangeSelection form = new ChangeSelection("PATH");
            form.Show();
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            ChangeSelection form = new ChangeSelection("KEY");
            form.Show();
        }
        
    }
}
