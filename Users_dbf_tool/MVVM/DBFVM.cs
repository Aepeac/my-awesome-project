﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Users_dbf_tool.Model
{
    public class DBFVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private ObservableCollection<string> _DBFS;
        private DBF _SelectedDbf;
        private DataSet ds;
        private bool update;

        public class DBF
        {
            public DBF(string path ="", string pswd = "", string name ="")
            {
                Path = path;
                Pswd = pswd;
                Name = name;
            }

            public string Path { get; set; }
            public string Pswd { get; set; }
            public string Name { get; set; }
        }

        public static DBF createDbf(string path = "", string pswd = "", string name = "")
        {
            return new DBF(path,pswd,name);
        }

        public DBFVM()
        {
            SelectedDbf = new DBF();
            _DBFS = new ObservableCollection<string>();
        }
        public bool Update
        {
            get
            {
                return this.update;
            }

            set
            {
                this.update = value;
                OnPropertyChanged();
            }
        }

        public DBF SelectedDbf
        {
            get
            {
                return this._SelectedDbf;
            }

            set
            {
                this._SelectedDbf = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<string> DBFS
        {
            get { return _DBFS; }
            set { this._DBFS = value; OnPropertyChanged(); }
        }
        public DataSet DS
        {
            get { return ds; }
            set { this.ds = value; OnPropertyChanged(); }
        }
        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
