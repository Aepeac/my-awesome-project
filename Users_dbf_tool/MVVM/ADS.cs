﻿using Advantage.Data.Provider;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Users_dbf_tool.Model
{
    public static class ADS
    {
        public static bool decrypted = false;
        public static string makeConnectionString(string datasource, string pswd)
        {
            // Create a new connection string builder

            AdsConnectionStringBuilder builder = new AdsConnectionStringBuilder();
            builder.DataSource = @datasource;
            builder.ServerType = "local";
            builder.EncryptionPassword = pswd;
            builder.TableType = "CDX";
            builder.LockMode = "proprietary";
            builder.SecurityMode = "ignorerights";
            builder.Shared = false;
            return builder.ConnectionString;
        }
        public static string makeConnectionString(string datasource)
        {
            // Create a new connection string builder

            AdsConnectionStringBuilder builder = new AdsConnectionStringBuilder();
            builder.DataSource = @datasource;
            builder.ServerType = "local";
            builder.TableType = "CDX";
            builder.LockMode = "proprietary";
            builder.SecurityMode = "ignorerights";
            builder.Shared = false;
            return builder.ConnectionString;
        }

        public static DataSet getDataTable(string tablename, string path, string pswd)
        {
            AdsConnection conn = new AdsConnection(Model.ADS.makeConnectionString(path, pswd));

            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Impossible de se connecter à ce dbf" + "\n" + ex.Message);
                return null;
            }

            AdsCommand cmd = conn.CreateCommand();
            cmd.CommandType = CommandType.TableDirect;
            cmd.CommandText = tablename;

            AdsDataAdapter da = new AdsDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds,"DatasDbf");
            conn.Close();
            return ds;
        }
        public static void UpdateData(string tablename, DataSet ds, string path, string pswd)
        {
            using (AdsConnection conn = new AdsConnection(makeConnectionString(path, pswd)))
            {
                conn.Open();
                AdsCommand cmd = conn.CreateCommand();
                cmd.CommandType = CommandType.TableDirect;
                cmd.CommandText = tablename;

                using (AdsDataAdapter da = new AdsDataAdapter(cmd))
                {

                    DataSet ds2 = new DataSet();
                    da.Fill(ds2, "DatasDbf");

                    DeleteDataRow(da);

                    foreach (DataRow row in ds.Tables["DatasDbf"].Rows)
                    {
                        DataRow nrow = ds2.Tables["DatasDbf"].NewRow();
                        nrow.ItemArray = row.ItemArray;
                        ds2.Tables["DatasDbf"].Rows.Add(nrow);
                    }
                    //ds2.AcceptChanges();
                    da.Update(ds2, "DatasDbf");
                    conn.Close();
                }
            }
        }

        private static void DeleteDataRow(AdsDataAdapter da)
        {
            AdsCommandBuilder cb = new AdsCommandBuilder(da);
            cb.RequirePrimaryKey = false;

            DataSet ds = new DataSet();
            da.Fill(ds, "DatasDbf");

            foreach (DataRow row in ds.Tables["DatasDbf"].Rows)
            {
                row.Delete();
            }
            //ds.AcceptChanges(); 
            da.Update(ds, "DatasDbf");
        }

        public static void decryptDbf(string path, string pswd, string tablename)
        {
            decrypted = true;
            AdsConnection conn = new AdsConnection(makeConnectionString(path));
            conn.Open();
            AdsExtendedReader reader_;
            AdsCommand cmd = new AdsCommand(tablename, conn);
            cmd.CommandType = CommandType.TableDirect;
            reader_ = cmd.ExecuteExtendedReader();
            try { reader_.DecryptTable(pswd); } catch {
                conn.Close();
                throw new Exception("Clé invalide");
            }
            conn.Close();
        }

        public static void encryptDbf(string path, string pswd, string tablename)
        {
            decrypted = false;
            AdsConnection conn = new AdsConnection(makeConnectionString(path));
            conn.Open();
            AdsExtendedReader reader_;
            AdsCommand cmd = new AdsCommand(tablename, conn);
            cmd.CommandType = CommandType.TableDirect;
            reader_ = cmd.ExecuteExtendedReader();
            reader_.EncryptTable(pswd);
            conn.Close();
        }
    }
}
